#include "path_symex_picker.h"

// check POSIX-compliance
#if defined(__linux__) || \
    defined(__FreeBSD_kernel__) || \
    defined(__GNU__) || \
    defined(__unix__) || \
    defined(__CYGWIN__) || \
    defined(__MACH__)

#include <unistd.h>
#include <sys/types.h>

path_symex_statest::iterator path_symex_picker_parallelt::internal_pick_state(
  path_symex_statest &further_states)
{
  path_symex_statest::iterator iter=m_path_symex_picker->pick_state(further_states);
  const path_symex_statet &state=*iter;

  if(1<further_states.size() && state.history->branch==path_symex_stept::BRANCH_TAKEN)
  {
    pid_t pid=-1;
    pid=fork();
    if(pid==0)
    {
      // child process
      path_symex_statest::iterator it=further_states.begin(), end=further_states.end();
      while(it!=end)
      {
        if(it!=iter)
          // iterators in std::list are stable
          it=further_states.erase(it);
        else
          ++it;
      }
    }
    else if (0<pid)
    {
      // parent process
      further_states.erase(iter);
      assert(!further_states.empty());
      iter=m_path_symex_picker->pick_state(further_states);
    }
  }

  return iter;
}

#endif

