/*******************************************************************\

Module: Interface for algorithms that pick states

        We support the following three implementations:

        1) Depth-first and breadth-first search strategies,
        2) Prune infeasible paths at branch statements,
        3) Parallel (only on POSIX-compliant systems)

        The last one (3) forks processes on BRANCH_TAKEN states.
        Control the maximum number of processes through `ulimit`.

Author: Alex Horn, alex.horn@cs.ox.ac.uk

\*******************************************************************/

#ifndef CPROVER_PATH_PICKER_H
#define CPROVER_PATH_PICKER_H

#include "path_symex_state.h"
#include <iterator>
#include "path_symex_merge.h"

#include <util/message.h>

class path_symex_pickert
{
public:
  inline path_symex_pickert()
  {
  }

  virtual ~path_symex_pickert()
  {
  }

  path_symex_statest::iterator pick_state(path_symex_statest &further_states)
  {
    assert(!further_states.empty());
    return internal_pick_state(further_states);
  }

private:
  /// \pre: further_states is not empty
  virtual path_symex_statest::iterator internal_pick_state(path_symex_statest &further_states) = 0;
};


/// Pick states in a depth-first search (DFS) manner
class path_symex_picker_dfst : public path_symex_pickert
{
public:
  inline path_symex_picker_dfst()
  {
  }

private:
  virtual path_symex_statest::iterator internal_pick_state(path_symex_statest &further_states)
  {
    return further_states.begin();
  }
};

/// Pick states in a breadth-first search (BFS) manner
class path_symex_picker_bfst : public path_symex_pickert
{
public:
  inline path_symex_picker_bfst()
  {
  }

private:
  virtual path_symex_statest::iterator internal_pick_state(path_symex_statest &further_states)
  {
    assert(!further_states.empty());
    path_symex_statest::iterator iter=further_states.end();
    std::advance(iter, -1);
    return iter;
  }
};


/// Pick states in a depth-first search (DFS) manner with selection.
class path_symex_picker_dfsmt : public path_symex_pickert
{
public:
  inline path_symex_picker_dfsmt()
  {
  }

private:
  virtual path_symex_statest::iterator internal_pick_state(path_symex_statest &further_states)
  {
	/* Start at the front (DFS) */
	path_symex_statest::iterator iter=further_states.begin();

	/* If it's a merge point, and we're not yet at the end, move on by one*/
	if(iter->is_merge_point() && iter != further_states.end())
		std::advance(iter, 1);

	/* If they were all merge points, choose the element at the back of the queue (arbitrarily.) */
	if(iter == further_states.end())
		std::advance(iter, -1);

	return iter;
  }
};



/*
/// Uses a SAT solver to prune infeasible paths
class path_symex_picker_prunet : public path_symex_pickert
{
public:
  inline path_symex_picker_prunet()
  {
  }

private:
  virtual path_symex_statest::iterator internal_pick_state(path_symex_statest &further_states)
  {
	  // to-do

  }
};
*/






// check POSIX-compliance
#if defined(__linux__) || \
    defined(__FreeBSD_kernel__) || \
    defined(__GNU__) || \
    defined(__unix__) || \
    defined(__CYGWIN__) || \
    defined(__MACH__)

/// Fork processes on BRANCH_TAKEN/BRANCH_NOT_TAKEN states
class path_symex_picker_parallelt : public path_symex_pickert
{
public:
  /// Transfers ownership of pointer to this class
  inline path_symex_picker_parallelt(path_symex_pickert* path_symex_picker)
  : m_path_symex_picker(path_symex_picker)
  {
    assert(m_path_symex_picker != 0);
  }

  ~path_symex_picker_parallelt()
  {
    delete m_path_symex_picker;
  }

private:
  virtual path_symex_statest::iterator internal_pick_state(path_symex_statest &further_states);

  path_symex_pickert* const m_path_symex_picker;
};

#endif

#endif
