/*******************************************************************\

Module: Path-based Symbolic Execution

Author: Daniel Kroening, kroening@kroening.com

\*******************************************************************/

#ifndef CPROVER_PATH_SEARCH_H
#define CPROVER_PATH_SEARCH_H

// check POSIX-compliance
#if defined(__linux__) || \
    defined(__FreeBSD_kernel__) || \
    defined(__GNU__) || \
    defined(__unix__) || \
    defined(__CYGWIN__) || \
    defined(__MACH__)
#define IS_POSIX
#endif

#include <util/time_stopping.h>

#include <goto-programs/safety_checker.h>

#include <path-symex/path_symex_state.h>
#include <path-symex/path_symex_picker.h>
#include <path-symex/path_symex_merge.h>


class path_searcht:public safety_checkert
{
public:
  explicit inline path_searcht(const namespacet &_ns):
    safety_checkert(_ns),
    show_vcc(false),
#ifdef IS_POSIX
    parallel_search_set(false),
#endif
    path_merging_set(false),
    merge_constants_set(false),
    infeasible_set(false),
    depth_limit_set(false), // no limit
    context_bound_set(false),
    unwind_limit_set(false),
    m_path_symex_picker(new path_symex_picker_dfst())
  {
  }

  ~path_searcht()
  {
    delete m_path_symex_picker;
  }

  virtual resultt operator()(
    const goto_functionst &goto_functions);

  /// Transfers ownership of pointer to this class

  /// \pre: picker != null
  void set_path_symex_picker(path_symex_pickert* path_symex_picker)
  {
    assert(path_symex_picker != 0);
    m_path_symex_picker = path_symex_picker;
  }

  path_symex_pickert* path_symex_picker() const
  {
    return m_path_symex_picker;
  }
    
  void set_depth_limit(unsigned limit)
  {
    depth_limit_set=true;
    depth_limit=limit;
  }

  void set_context_bound(unsigned limit)
  {
    context_bound_set=true;
    context_bound=limit;
  }

  void set_unwind_limit(unsigned limit)
  {
    unwind_limit_set=true;
    unwind_limit=limit;
  }

  bool show_vcc;

#ifdef IS_POSIX
  bool parallel_search_set;
#endif
  bool path_merging_set, merge_constants_set, infeasible_set;
  
  // statistics
  unsigned number_of_dropped_states;
  unsigned number_of_paths;
  unsigned number_of_steps;
  unsigned number_of_VCCs;
  unsigned number_of_VCCs_after_simplification;
  unsigned number_of_failed_properties;
  unsigned number_of_infeasible_paths;
  unsigned number_of_feasible_paths;
  absolute_timet start_time;
  time_periodt sat_time, state_merge_time;

  enum statust { NOT_REACHED, PASS, FAIL };

  struct property_entryt
  {
    statust status;
    irep_idt description;
    goto_tracet error_trace;
  };
  
  typedef std::map<irep_idt, property_entryt> property_mapt;
  property_mapt property_map;

protected:
  typedef path_symex_statet statet;

  // State queue. Iterators are stable.
  typedef std::list<statet> queuet;
  queuet queue;

#ifdef IS_POSIX
  // blocks until child processes have terminated
  int await();

  // returns whether at least one child process has terminated
  bool try_await();
#endif

  queuet::iterator pick_state();
  
  bool execute(queuet::iterator state, const namespacet &);
  
  void check_assertion(statet &state, const namespacet &);
  void do_show_vcc(statet &state, const namespacet &);
  
  bool drop_state(const statet &state) const;
  
  void report_statistics();
  
  void initialize_property_map(
    const goto_functionst &goto_functions);

  unsigned depth_limit;
  unsigned context_bound;
  unsigned unwind_limit;


  bool depth_limit_set, context_bound_set, unwind_limit_set;

  path_symex_merget merger;
  // never null
  path_symex_pickert* m_path_symex_picker;
};

#endif
