/*******************************************************************\

Module: Path-based Symbolic Execution Merging

Author: Daniel Kroening, kroening@kroening.com

\*******************************************************************/

#include <path-symex/path_symex.h>
#include "path_symex_merge.h"

/*******************************************************************\

Function: path_symex_merget::constant_only(
			path_symex_statet &state,
			path_symex_statet &negated_state)


  Inputs: Two states.

 Outputs: Boolean value representing whether both only contain
 	 	  constant values in their set of differences.

 Purpose: Given two iterators, this will return TRUE if the
 	 	  differences in their memory map are all constant.

		  Otherwise, false.

\*******************************************************************/

bool path_symex_merget::constant_only(path_symex_statet &state, path_symex_statet &negated_state)
{
  for(var_mapt::id_mapt::iterator
      it=state.var_map.id_map.begin();
      it!=state.var_map.id_map.end();
      it++)
  {
    if( !(state.get_var_state(it->second).value == negated_state.get_var_state(it->second).value) &&
        ((state.get_var_state(it->second).value.is_constant()) && negated_state.get_var_state(it->second).value.is_constant())) {
      return true;
    }
  }

  return false;
}

/*******************************************************************\

Function: path_symex_merget::suitable_to_merge(
			queuet::iterator &iterator_1,
			queuet::iterator &iterator_2,
			exprt &guard,
			bool &inner_negated)

  Inputs: Two iterators, a guard reference and a bool reference.

 Outputs: Boolean value representing whether these two iterators are suitable to be merged.

 Purpose: Given two iterators, this will return TRUE if they
 	 	  can be safely merged at the last guard location (see above)
 	 	  or FALSE if this is not the case.

 	 	  If it returns TRUE, the guard reference will be updated to
 	 	  the value of the guard when the program flows diverged.

 	 	  The inner_negated boolean refers to whether it is the first
 	 	  or second iterator that is the negation of the other.

\*******************************************************************/

bool path_symex_merget::suitable_to_merge(queuet::iterator &iterator_1, queuet::iterator &iterator_2, exprt &guard, bool &inner_negated)
{
  /* These are effectively done elsewhere, but are repeated for safety. */
  if(iterator_1 == iterator_2 ||
      iterator_1->pc() != iterator_2->pc() ||
      iterator_1->inside_atomic_section != iterator_2->inside_atomic_section ||
      iterator_1->get_current_thread() != iterator_2->get_current_thread()
  ) {
    return false;
  }

  /* then guard checks. */

  /* then equality checks (if necessary) */

  path_symex_step_reft history_1 = iterator_1->history;
  path_symex_step_reft history_2 = iterator_2->history;

  exprt guard_1, guard_2, negated_guard;

  while(history_1->guard.is_nil() && !history_1.is_nil())
    --history_1;

  while(history_2->guard.is_nil() && !history_2.is_nil())
    --history_2;

  if((history_1->guard.is_nil() && history_1.is_nil()) ||
      (history_2->guard.is_nil() && history_2.is_nil())) {
    // If either of them were fully traversed without guards, do not merge.
    return false;
  }

  guard_1 = history_1->guard;
  guard_2 = history_2->guard;

  /* One of them must be the NOT of the other. */
  if(guard_1.id() == ID_not && guard_1.op0() == guard_2) {
    negated_guard = guard_1;
    guard = guard_2;
    inner_negated = false;
  } else if(guard_2.id() == ID_not && guard_2.op0() == guard_1) {
    negated_guard = guard_2;
    guard = guard_1;
    inner_negated = true;
  } else {
    return false;
  }

  if(negated_guard.op0() != guard) {
    return false;
  }

  if(history_1.is_nil() || history_2.is_nil()) {
    return (history_1.is_nil() && history_2.is_nil());
  }

  --history_1;
  --history_2;

  while(!history_1.is_nil() && !history_2.is_nil()) {
    if(!(history_1->guard == history_2->guard)) {
      return false;
    }
    --history_1;
    --history_2;
  }

  if(history_1.is_nil() != history_2.is_nil()) {
    return false;
    // work still to do in one of the paths.
  }

  /* Constant check */
  if(!merge_constants) {
    if(constant_only(*iterator_1, *iterator_2)) {
      return false;
    }
  }

  return true;
}

/*******************************************************************\

Function: path_symex_merget::merge_states(std::list<statet> &queue)

  Inputs: The queue, a list of states and a single state iterator.

 Outputs: A boolean representing if any states were merged, and
 	 	  the queue is directly modified to represent this merge.

 Purpose: Given the queue, calculate firstly if any two states
 	 	  can be merged, and if they can: merge them in place.

 	 	  Return true as soon as any merge occurs, this can be placed
 	 	  in a while loop that executes repeatedly until no further
 	 	  merges can occur.

\*******************************************************************/

bool path_symex_merget::merge_states(queuet &queue, queuet::iterator& state)
{
  if(queue.size() < 2) {
    return false;
  }

  for (queuet::iterator iterator = queue.begin(); iterator != queue.end(); ++iterator) {

    if(state == iterator)
      continue;

    exprt guard;
    bool inner_negated;

    /* Immediate PC check as this is the quickest way to stop further calculation. */
    if(state->pc() == iterator->pc() && suitable_to_merge(state, iterator, guard, inner_negated)) {
      /* The first argument here is modified to BE the merged state. */
      if(create_merged_state(inner_negated ? *state : *iterator,
          inner_negated ? *iterator : *state, guard))
      {
        merge_locations.push_back(iterator->pc());

        /* Remove second argument state, it has been merged into the first one. */
        queue.erase(inner_negated ? iterator : state);

        return true;
      }
    }
  }
  return false;
}

/*******************************************************************\

Function: path_symex_merget::create_merged_state(
			  path_symex_statet &state,
			  path_symex_statet &negated_state,
			  exprt guard)

  Inputs: Two states, one of which the last guard is the negation
          of the other.
          The guard itself.

 Outputs: A boolean representing successful execution, and the
		  modification of the first variable which is transformed
		  into the new merged state.

 Purpose: Given two states, merge them by combining the differences
          in their data stores via ternary operators.

\*******************************************************************/

bool path_symex_merget::create_merged_state(path_symex_statet &state, path_symex_statet &negated_state, exprt &guard) {
  for(var_mapt::id_mapt::iterator
      it=state.var_map.id_map.begin();
      it!=state.var_map.id_map.end();
      it++)
  {

    if(! (state.get_var_state(it->second).value == negated_state.get_var_state(it->second).value)) {
      if_exprt phi_node;
      if(state.get_var_state(it->second).value.is_not_nil() && negated_state.get_var_state(it->second).value.is_not_nil()) {

        phi_node = if_exprt(guard, state.get_var_state(it->second).value, negated_state.get_var_state(it->second).value,
            state.get_var_state(it->second).value.type());

      } else if(state.get_var_state(it->second).value.is_not_nil() && negated_state.get_var_state(it->second).value.is_nil()) {

        phi_node = if_exprt(guard, state.get_var_state(it->second).value, it->second.ssa_symbol(), state.get_var_state(it->second).value.type());

      } else if(negated_state.get_var_state(it->second).value.is_not_nil() && state.get_var_state(it->second).value.is_nil()) {

        phi_node = if_exprt(guard, it->second.ssa_symbol(), negated_state.get_var_state(it->second).value,
            negated_state.get_var_state(it->second).value.type());

      } else {
        assert(0); // They're both the same via the previous IF, so if they're both NIL, they're the same.
        // This should not be reached.
      }
      state.get_var_state(it->second).value = phi_node;
    }
  }

  /* Now update the history to remove the difference. */
  path_symex_step_reft s = state.history;
  bool found = false;
  while(!s.is_nil() && !found)
  {
    if(s.get_guard().is_not_nil() && s.get_guard() == guard) {
      s.get_guard().make_nil();
      found = true;
    } else {
      --s;
    }
  }

  assert(found);
  assert(s.get_guard().is_nil());

  return true;
}

