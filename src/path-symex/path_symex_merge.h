/*
 * path_symex_merge.h
 *
 *  Created on: 22 Apr 2015
 *      Author: danlle
 */

#ifndef PATH_SYMEX_PATH_SYMEX_MERGE_H_
#define PATH_SYMEX_PATH_SYMEX_MERGE_H_

#include <path-symex/path_symex_state.h>

/* Copied from other class */
typedef path_symex_statet statet;
typedef std::list<statet> queuet;

class path_symex_merget {
public:
  path_symex_merget(): merge_constants(false), number_of_merge_calls(0) {};

  inline bool do_merge_states(queuet &queue, queuet::iterator& state) {
    number_of_merge_calls++;
    return merge_states(queue, state);
  }

  inline void set_merge_constants(bool setting) {
    merge_constants = setting;
  }

  inline std::list<loc_reft> &get_merge_locations() {
    return merge_locations;
  }

  inline unsigned int get_merge_count() {
    return (unsigned int) merge_locations.size();
  }

  inline unsigned int get_merge_calls() {
    return number_of_merge_calls;
    /* Not currently used, but maintained for later possible use. */
  }

protected:
  bool merge_constants;
  unsigned int number_of_merge_calls;
  std::list<loc_reft> merge_locations;

  /* A function to call on queue, and one other element. */
  bool merge_states(queuet &queue, queuet::iterator &state);

  /* Given two states, can they be merged? */
  bool suitable_to_merge(queuet::iterator &iterator_1, queuet::iterator &iterator_2, exprt &guard, bool &inner_negated);

  /* Given two states that can be merged, merge them. */
  bool create_merged_state(statet &state, statet &negated_state, exprt &guard);

  /* Useful methods that minimise code duplication. */
  bool constant_only(path_symex_statet &state, path_symex_statet &negated_state);
};

#endif /* PATH_SYMEX_PATH_SYMEX_MERGE_H_ */
